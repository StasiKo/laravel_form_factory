require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';

Vue.use(VueRouter);
Vue.use(VueMaterial);

import App from './components/App.vue';
import MemberCreatePage from './components/pages/member/create/index';
import OrderCreatePage from './components/pages/order/create/index';
import MemberEditPage from './components/pages/member/edit/_id';
import MemberShowPage from './components/pages/member/_id';
import MemberList from './components/pages/members/index';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/member/create',
            name: 'create',
            component: MemberCreatePage
        },
        {
            path: '/member/edit/:id',
            name: 'edit',
            component: MemberEditPage
        },
        {
            path: '/member/show/:id',
            name: 'show',
            component: MemberShowPage
        },
        {
            path: '/',
            name: 'list',
            component: MemberList,
        },
        {
            path: '/order/create',
            name: 'create',
            component: OrderCreatePage
        },
    ],
});

const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app')
