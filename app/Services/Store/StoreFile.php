<?php

namespace App\Services\Store;

use App\Models\Order;
use Illuminate\Support\Facades\Storage;

class StoreFile
{
    protected $fileName = 'store_file';
    public function save($data)
    {
        $content = [
            'name' => $data['name'],
            'phone' => $data['phone'],
            'text' => $data['text']
        ];

        Storage::disk('local')->put("{$this->fileName}.txt", json_encode($content));
        return ['path' => Storage::disk('local')->path($this->fileName)];
    }

}
