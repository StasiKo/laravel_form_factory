<?php

namespace App\Services\Store;

use App\Models\Order;

class StoreDb
{
    public function save($data)
    {
        $order = Order::create(
            [
                'name' => $data['name'],
                'phone' => $data['phone'],
                'text' => $data['text']
            ]
        );
        return $order;
    }
}
