<?php

namespace App\Services\Factory;

use App\Services\Store\StoreDb;
use App\Services\Store\StoreFile;

class FormFactory extends AbstractFormFactory
{
    protected $mapping = [
        'storeDb' => StoreDb::class,
        'storeFile' => StoreFile::class
    ];

    public function create($type)
    {
        $className = $this->mapping[$type];
        return new $className;
    }
}
