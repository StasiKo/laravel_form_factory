<?php

namespace App\Services\Factory;

abstract class AbstractFormFactory
{
    protected function getType()
    {
        return get_class($this);
    }
}
