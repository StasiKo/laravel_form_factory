<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:12',
            'phone' => 'required|min:2|max:12',
            'text' => 'required|min:2',
            'saveType' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => ['required' => 'error'],
            'name.min:2' => ['minLength' => 'error'],
            'name.max:12' => ['maxLength' => 'error'],

            'phone.required' => ['required' => 'error'],
            'phone.min:2' => ['minLength' => 'error'],
            'phone.max:12' => ['maxLength' => 'error'],

            'text.required' => ['required' => 'error'],
            'text.min:2' => ['minLength' => 'error']
        ];
    }
}
