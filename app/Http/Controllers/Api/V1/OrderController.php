<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Models\Order;
use Illuminate\Http\JsonResponse;
use App\Services\Factory\FormFactory;

class OrderController extends Controller
{
    /**
     * Create order
     *
     * @param OrderRequest $request
     * @return JsonResponse
     */
    public function create(OrderRequest $request)
    {
        $data = $request->all();

        $factory = new FormFactory();
        $saveInstance = $factory->create($data['saveType']);

        return response()->json($saveInstance->save($data));
    }

}
