<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\MemberRequest;
use App\Models\Member;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * Display a listing of the members.
     *
     * @return JsonResponse
     */
    public function list()
    {
        return response()->json(Member::all());
    }

    /**
     * Get member
     *
     * @param Request $request
     * @param string  $id
     * @return JsonResponse
     */
    public function get(Request $request, $id)
    {
        return response()->json(Member::findOrFail($id));
    }

    /**
     * Create member
     *
     * @param MemberRequest $request
     * @return JsonResponse
     */
    public function create(MemberRequest $request)
    {
        $data = $request->all();
        $member = Member::create(
            [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'phone' => $data['phone'],
                'email' => $data['email'],
                'password' => $data['password'],
                'agree' => $data['agree'],
                'subscribe' => $data['subscribe'],
                'on_vacation' => false
            ]
        );

        return response()->json($member);
    }


    /**
     * Update member
     *
     * @param Request $request
     * @param string  $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $member = Member::findOrFail($id);
        $member->update($data);

        return response()->json(Member::findOrFail($id));
    }

    /**
     * Delete member
     *
     * @param Request $request
     * @param string  $id
     * @return JsonResponse
     */
    public function delete(Request $request, $id)
    {
        $member = Member::findOrFail($id);
        $member->delete();

        return response()->json($member);
    }

}
