<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('member.list');

Route::get('/member/create', function () {
    return view('index');
})->name('member.create');

Route::get('/member/show/{id}', function () {
    return view('index');
})->name('member.show');

Route::get('/member/edit/{id}', function () {
    return view('index');
})->name('member.edit');

Route::get('/order/create', function () {
    return view('index');
})->name('order.create');
