<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ApiOrderTest extends TestCase
{
    use DatabaseMigrations;

    protected $orderData = [
        'name' => 'test',
        'phone' => 'phone test',
        'text' => 'test text',
        'saveType' => 'storeDb'
    ];

    /**
     * A test create route.
     *
     * @return void
     */
    public function test_create()
    {
        $data = $this->orderData;
        $response = $this->call('POST', route('api.order.create'), $data);
        $decoded = json_decode($response->content(), true);
        $this->assertEquals('test', $decoded['name']);

        $data['saveType'] = 'storeFile';
        $responseStoreFile = $this->call('POST', route('api.order.create'), $data);
        $decodedStoreFile = json_decode($responseStoreFile->content(), true);
        $this->assertArrayHasKey('path', $decodedStoreFile);
    }
}
